import { createElement } from "./dom-helpers.mjs";

function userElement(username, you = '') {

    const userDiv = createElement({
        tagName: "div",
        className: "user-wrapper",
        attributes: {}
    });

    userDiv.innerHTML = `
<div class="user-info">
<div class="user-status-${username} ready-status-red status"></div><span class="user-name">  ${username + you}</span>
</div>
<div class="progress-bar">
<div class="user-progress-${username} user-progress"></div>
</div>
    `;

    return userDiv;
}

export { userElement }