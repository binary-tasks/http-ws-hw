import { addClass, removeClass, createElement } from "./dom-helpers.mjs";

function roomElement(usersCount, roomName, username, socket) {
    const roomDiv = createElement({
        tagName: "div",
        className: "room",
        attributes: {}
    });

    roomDiv.innerHTML = `
    <span>${usersCount} users connected</span>
    <h4>${roomName}</h4>
    `;

    const roomJoinButton = createElement({
        tagName: "button",
        className: "join-btnt",
        attributes: { id: roomName }
      });

      roomJoinButton.innerText = "Join";

      const onJoinRoom = () => {
        socket.emit("JOIN_ROOM", { roomName, username });
      };
    
      roomJoinButton.addEventListener("click", onJoinRoom);

      roomDiv.appendChild(roomJoinButton);

    return roomDiv;
}

export { roomElement }