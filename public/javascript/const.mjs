const createRoomBtn = document.getElementById("add-room-btn");
const roomsPage = document.getElementById("rooms-page");
const gamePage = document.getElementById("game-page");
const roomsGrid = document.getElementById("rooms-grid");
const roomNameLabel = document.getElementById("room-name");
const quitRoomBtn = document.getElementById("quit-room-btn");
const usersContainer = document.getElementById("users-container");
const textDone = document.getElementById('done');
const textNext = document.getElementById('next');
const textUndone = document.getElementById('undone');
const readyBtn = document.getElementById('ready-btn');
const timer = document.getElementById("timer");
const gameTimer = document.getElementById("game-timer");

export {
    createRoomBtn,
    roomsPage,
    gamePage,
    roomsGrid,
    roomNameLabel,
    quitRoomBtn,
    usersContainer,
    textDone,
    textNext,
    textUndone,
    readyBtn,
    timer,
    gameTimer
}