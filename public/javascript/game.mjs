import { addClass, removeClass, createElement } from "../javascript/helpers/dom-helpers.mjs";
import { viewRoom } from "./helpers/view-room.mjs"
import { setUserStatus } from "./helpers/set-user-status.mjs"
import {
  createRoomBtn,
  roomsPage,
  gamePage,
  roomsGrid,
  roomNameLabel,
  quitRoomBtn,
  readyBtn,
  timer,
  gameTimer,
  textDone,
  textNext,
  textUndone
} from "./const.mjs";

const username = sessionStorage.getItem("username");
let roomName = "";
let text = "the good";

if (!username) {
  window.location.replace("/login");
}

const socket = io("", { query: { username } });

function endGame(count) {
  alert(count);
}

createRoomBtn.addEventListener('click', () => {
  roomName = prompt("Input Room Name:");
  socket.emit("CREATE_ROOM", { roomName, username });
  removeClass(gamePage, "display-none");
  addClass(roomsPage, "display-none");
})

quitRoomBtn.addEventListener('click', () => {
  socket.emit("LEAVE_ROOM", ({ roomName, username }));
  addClass(gamePage, "display-none");
  removeClass(roomsPage, "display-none");
  roomName = "";
})

readyBtn.addEventListener('click', () => {
  let status = false;
  if (readyBtn.innerText === "READY") {
    status = true;
    readyBtn.innerText = 'NOT READY'
  } else {
    status = false;
    readyBtn.innerText = 'READY'
  }
  setUserStatus(status, username);
  socket.emit("USER_IS_READY", { status, roomId: roomName, user: username });
})

const roomElement = (usersCount, roomId) => {
  const roomDiv = createElement({
    tagName: "div",
    className: "room",
    attributes: {}
  });

  roomDiv.innerHTML = `
  <span>${usersCount} users connected</span>
  <h4>${roomId}</h4>
  `;

  const roomJoinButton = createElement({
    tagName: "button",
    className: "join-btnt",
    attributes: { id: roomId }
  });

  roomJoinButton.innerText = "Join";

  const onJoinRoom = () => {
    socket.emit("JOIN_ROOM", { roomName: roomId, username });
  };

  roomJoinButton.addEventListener("click", onJoinRoom);

  roomDiv.appendChild(roomJoinButton);

  return roomDiv;
}

socket.on("DUBLICATE_USER", username => {
  sessionStorage.removeItem("username");
  window.location.replace("/login");
  alert(`Usrename ${username} exist`);
});

socket.on("ROOMS_UPDATE", rooms => {
  roomsGrid.innerHTML = '';
  rooms.forEach(room => {
    const roomCard = roomElement(room[1], room[0]);
    roomsGrid.appendChild(roomCard);
  });
});

socket.on("DUBLICATE_ROOM", roomId => {
  alert(`Room ${roomId} exist`);
});

socket.on("MAX_TO_ROOM", () => {
  alert(`Maximum users for this room`);
});

socket.on("JOIN_ROOM_DONE", ({ roomId, users }) => {
  removeClass(gamePage, "display-none");
  addClass(roomsPage, "display-none");
  roomNameLabel.innerHTML = roomId;
  roomName = roomId;
  viewRoom(users, username);
});

socket.on("USER_IS_READY", ({ status, user }) => setUserStatus(status, user));

socket.on("TIMER_START", () => {
  addClass(quitRoomBtn, "display-none");
  addClass(readyBtn, "display-none");
});
socket.on("TIMER_GO", tik => timer.innerHTML = tik);
socket.on("GAME_START", () => {
  addClass(timer, "display-none");
  startGame(text, endGame, socket);
});
socket.on("GAME_TIMER_GO", tik => {
  gameTimer.innerHTML = tik;
});

socket.on("USER_PROGRESS", ({ progress, user, roomId }) => {
  const userProgress = document.getElementsByClassName(`user-progress-${user}`)[0];
  userProgress.style.width = `${progress * (100 / text.length)}%`;
  if ((progress * (100 / text.length) >= 100)) {
    socket.emit("USER_TEXT_DONE", ({ user, roomId }));
    userProgress.style.backgroundColor = "blue";
  }

});

socket.on("GAME_END", (gameResult) => {
  alert(gameResult);
});


function startGame(gameText, endGame) {
  document.addEventListener('keyup', keyboardEventsHandle);

  textUndone.innerHTML = gameText;

  gameText += " ";

  let GameCount = 0;

  function keyboardEventsHandle(e) {
    if (e.key === gameText[GameCount]) {
      GameCount++;
      socket.emit("USER_TYPE", { progress: GameCount, roomId: roomName, user: username });
      if (GameCount === gameText.length) {
        document.removeEventListener('keyup', keyboardEventsHandle);
      }
      textDone.innerHTML = gameText.slice(0, GameCount);
      textUndone.innerHTML = gameText.slice(GameCount + 1);
      if (GameCount < gameText.length)
        textNext.innerHTML = gameText[GameCount];
    }
  }
}