class Db {
  constructor(){
    this.rooms = new Map();
  }

  _makeUser(isReady, progress, isTextDone) {
    return {
      isReady,
      progress,
      isTextDone
    }
  };

  setRoom(roomId, user) {
    const users = new Map();
    this.rooms.set(roomId, users);
    this.addUserToRoom(roomId, user);
  }

  getRooms() {
    let result = [];
    for (let room of this.rooms.keys()) {
      let item = [];
      item.push(room); item.push(this.getRoomUsers(room).length);
      result.push(item);
    }
    return result;
  }

  getRoomUsers(roomId) {
    const users = this.rooms.get(roomId);
    let result = [];
    for (let user of users.keys()) {
      result.push(user);
    }
    return result;
  }

  addUserToRoom(roomId, user) {
    const users = this.rooms.get(roomId);
    users.set(user, this._makeUser(false, 0, false));
    this.rooms.set(roomId, users);
  }

  delUserFromRoom(roomId, user) {
    const users = this.rooms.get(roomId);
    users.delete(user);
    this.rooms.set(roomId, users);
  }

  setUserStatus(status, roomId, user) {
    const users = this.rooms.get(roomId);
    const userToChange = users.get(user);
    userToChange.isReady = status;
    users.set(user, userToChange);
    this.rooms.set(roomId, users);
  }

  isAllReadyInRoom(roomId) {
    const users = this.rooms.get(roomId);
    let result = true;
    for (let user of users.values()) {
      if (user.isReady === false) result = false;
    }
    return result;
  }

  isAllTextDone(roomId) {
    const users = this.rooms.get(roomId);
    let result = true;
    for (let user of users.values()) {
      if (user.isTextDone === false) result = false;
    }
    return result;
  }

  setProgress(progress, roomId, user) {
    const users = this.rooms.get(roomId);
    const userToChange = users.get(user);
    userToChange.progress = progress;
    users.set(user, userToChange);
    this.rooms.set(roomId, users);
  }

  setTextDone(roomId, user) {
    const users = this.rooms.get(roomId);
    const userToChange = users.get(user);
    userToChange.isTextDone = true;
    users.set(user, userToChange);
    this.rooms.set(roomId, users);
  }

  getGameResult(roomId) {
    const usersArr = this.getRoomUsers(roomId);
    const users = this.rooms.get(roomId);
    const allUsers = [];
    usersArr.forEach(user => {
      allUsers.push(user);
      allUsers.push(users.get(user).progress);
    });
    return allUsers;
  }

  hasRoom(roomId) {
    return (this.rooms.has(roomId));
  }

  delRoom(roomId) {
    this.rooms.delete(roomId);
  }

}

// class Db {
//   constructor() {
//     this.rooms = new Map();
//   }

//   setRoom(room, user) {
//     const users = new Set();
//     users.add(user);
//     this.rooms.set(room, users);
//   }

//   hasRoom(room) {
//     return (this.rooms.has(room));
//   }

//   delRoom(room) {
//     if (this.rooms.has(room)) this.rooms.delete(room);
//   }

//   getRoomUsers(room) {
//     const users = [];
//     this.rooms.get(room).forEach(el => { users.push(el) });
//     return users;
//   }

//   addUserToRoom(room, user) {
//     if (this.rooms.has(room)) {
//       this.rooms.set(room,
//         this.rooms.get(room).add(user));
//     }
//   }

//   delUserFromRoom(room, user) {
//     if (this.rooms.has(room)) {
//       const users = this.rooms.get(room);
//       users.delete(user);
//       this.rooms.set(room, users);
//     }
//   }

//   getRooms() {
//     const result = [];
//     for (let room of this.rooms.keys()){
//       const item = [];
//       item.push(room); item.push(this.rooms.get(room).size);
//       result.push(item);
//     }

//     return result;
//   }
// }

const db = new Db();

export { db }
