import * as config from "./config";
import { db } from "./helpers/db.mjs"

const users = new Set();

function startTimer(sec, roomId, socket) {
  let tik = sec;
  let timerId = setInterval(() => {
    socket.to(roomId).emit("TIMER_GO", tik);
    socket.emit("TIMER_GO", tik);
    tik--;
  }, 1000);
  setTimeout(() => {
    clearInterval(timerId);
    socket.to(roomId).emit("GAME_START");
    socket.emit("GAME_START");
    startGame(config.SECONDS_FOR_GAME, roomId, socket);
  }, (sec + 1) * 1000);
}

function startGame(sec, roomId, socket) {
  let tik = sec;
  let timerId = setInterval(() => {
    socket.to(roomId).emit("GAME_TIMER_GO", tik);
    socket.emit("GAME_TIMER_GO", tik);
    tik--;
  }, 1000);
  setTimeout(() => {
    clearInterval(timerId);
    const gameResult = db.getGameResult(roomId);
    socket.to(roomId).emit("GAME_END", gameResult);
    socket.emit("GAME_END", gameResult);
  }, (sec + 1) * 1000);
}

export default io => {
  io.on("connection", socket => {
    const username = socket.handshake.query.username;
    if (users.has(username)) {
      socket.emit("DUBLICATE_USER", username);
    } else {
      users.add(username);
      socket.emit("ROOMS_UPDATE", db.getRooms());
    }
    socket.on("CREATE_ROOM", ({ roomName, username }) => {
      if (db.hasRoom(roomName)) {
        socket.emit("DUBLICATE_ROOM", roomName);
      } else {
        db.setRoom(roomName, username);
        socket.join(roomName);
        const users = [];
        users.push(username);
        socket.emit("JOIN_ROOM_DONE", { roomId: roomName, users });
        socket.broadcast.emit("ROOMS_UPDATE", db.getRooms());
      }
    })
    socket.on("JOIN_ROOM", ({ roomName, username }) => {
      if (db.getRoomUsers(roomName).length == config.MAXIMUM_USERS_FOR_ONE_ROOM) {
        socket.emit("MAX_TO_ROOM");
      } else {
        socket.join(roomName);
        db.addUserToRoom(roomName, username);
        const users = db.getRoomUsers(roomName);
        socket.emit("JOIN_ROOM_DONE", { roomId: roomName, users });
        socket.broadcast.emit("ROOMS_UPDATE", db.getRooms());
        socket.to(roomName).emit("JOIN_ROOM_DONE", { roomId: roomName, users });
      }
    })
    socket.on("LEAVE_ROOM", ({ roomName, username }) => {
      socket.leave(roomName);
      let users = db.getRoomUsers(roomName);
      db.delUserFromRoom(roomName, username);
      if (users.length === 1) {
        db.delRoom(roomName);
        socket.emit("ROOMS_UPDATE", []);
      } else {
        if (db.isAllReadyInRoom(roomName)) {
          socket.to(roomName).emit("TIMER_START");
          socket.emit("TIMER_START");
          startTimer(config.SECONDS_TIMER_BEFORE_START_GAME, roomName, socket);
          socket.broadcast.emit("ROOMS_UPDATE", db.getRooms());
          socket.emit("ROOMS_UPDATE", db.getRooms());
          users = db.getRoomUsers(roomName);
          socket.to(roomName).emit("JOIN_ROOM_DONE", { roomId: roomName, users });
        }
      }
    })
    socket.on("USER_IS_READY", ({ status, roomId, user }) => {
      db.setUserStatus(status, roomId, user);
      socket.to(roomId).emit("USER_IS_READY", { status, user });
      if (db.isAllReadyInRoom(roomId)) {
        socket.to(roomId).emit("TIMER_START");
        socket.emit("TIMER_START");
        startTimer(config.SECONDS_TIMER_BEFORE_START_GAME, roomId, socket);
      }
    });
    socket.on("USER_TYPE", ({ progress, roomId, user }) => {
      db.setProgress(progress, roomId, user);
      socket.to(roomId).emit("USER_PROGRESS", { progress, user, roomId });
      socket.emit("USER_PROGRESS", { progress, user, roomId });
    });
    socket.on("USER_TEXT_DONE", ({ user, roomId }) => {
      db.setTextDone(roomId, user);
      if (db.isAllTextDone(roomId)) {
        const gameResult = db.getGameResult(roomId);
        socket.to(roomId).emit("GAME_END", gameResult);
        socket.emit("GAME_END", gameResult);
      }
    });
  });
};
